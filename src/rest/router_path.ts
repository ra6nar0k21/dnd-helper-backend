import { NextFunction, Request, Response, Router } from 'express';

/**
 * Basic HTTPS Request types
 */
enum RouteMethodType {
    GET,
    POST,
    PUT,
    DELETE,
}

type RouteHandlerType = (req: Request, res: Response, next: NextFunction) => void | Promise<void>;

interface IRoute {
    /**
     * The path of the route
     */
    path: string;

    /**
     * The Method of the route
     */
    method: RouteMethodType;

    /**
     * The handler when the route is requested
     */
    handler: RouteHandlerType;

    /**
     * Any middlewares which get called before the handler
     */
    local_middleware?: Array<RouteHandlerType>;
}

abstract class RouterPath {
    public abstract path: string;

    protected abstract readonly routes: Array<IRoute>;

    /**
     * Returns a router with all paths applied
     */
    public get_router(): Router {
        const router = Router();

        for (const route of this.routes) {
            if (route.local_middleware !== undefined) {
                for (const mw of route.local_middleware) {
                    router.use(mw);
                }
            }

            switch (route.method) {
                case RouteMethodType.GET:
                    router.get(route.path, route.handler);
                    break;
                case RouteMethodType.PUT:
                    router.put(route.path, route.handler);
                    break;
                case RouteMethodType.POST:
                    router.post(route.path, route.handler);
                    break;
                case RouteMethodType.DELETE:
                    router.delete(route.path, route.handler);
                    break;
                default:
                    break;
            }
        }

        return router;
    }
}

export { RouterPath, IRoute, RouteHandlerType, RouteMethodType };
