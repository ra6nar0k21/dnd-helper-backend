import { ChangelogDocument } from './changelog_document';
import { model, Model, Schema } from 'mongoose';

type ChangelogModel = Model<ChangelogDocument>

const changelogScheme: Schema = new Schema({
    title: String,
    content: String,
    version: String,
});

const ChangelogObj: ChangelogModel = model<ChangelogDocument>('changelogs', changelogScheme, 'changelogs');

export { ChangelogObj, ChangelogModel as IChangelogModel };
