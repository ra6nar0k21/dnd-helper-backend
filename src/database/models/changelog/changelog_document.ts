import { Document } from 'mongoose';

interface Changelog {
    title: string;
    content: string;
    version: string;
}

interface ChangelogDocument extends Document, Changelog {
}

export { Changelog, ChangelogDocument };
