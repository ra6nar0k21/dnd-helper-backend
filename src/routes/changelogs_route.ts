import { IRoute, RouteMethodType, RouterPath } from '../rest/router_path';
import { Request, Response } from 'express';
import { ChangelogObj } from '../database/models/changelog/changelog_model';
import { Changelog, ChangelogDocument } from '../database/models/changelog/changelog_document';

class ChangelogsRoute extends RouterPath {
    path = '/changelogs';

    protected readonly routes: Array<IRoute> = [
        {
            path: '/search/:version',
            method: RouteMethodType.GET,
            handler: this.search_version,
        },
        {
            path: '/add',
            method: RouteMethodType.POST,
            handler: this.add,
        },
    ];

    async search_version(req: Request, res: Response): Promise<void> {
        const version = decodeURI(req.params.version).toLowerCase();
        const changelogs = await ChangelogObj.findOne({
            version,
        }).select({ __v: 0 }).lean().exec();
        res.send(changelogs);
    }

    async add(req: Request, res: Response): Promise<void> {
        const data: Changelog = req.body;
        const changelog: ChangelogDocument = new ChangelogObj();

        changelog.title = data.title;
        changelog.version = data.version;
        changelog.content = data.content;

        changelog.save().then(saved => {
            if (saved === changelog) {
                res.json(changelog);
            } else {
                res.json({ success: false });
            }
        });
    }
}

export { ChangelogsRoute };
