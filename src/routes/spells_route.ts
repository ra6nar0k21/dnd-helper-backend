import { IRoute, RouteMethodType, RouterPath } from '../rest/router_path';
import { Request, Response } from 'express';
import { SpellObj } from '../database/models/spell/spell_model';
import { Spell, SpellDocument } from '../database/models/spell/spell_document';

class SpellsRoute extends RouterPath {
    path = '/spells';

    protected readonly routes: Array<IRoute> = [
        {
            path: '/get_all',
            method: RouteMethodType.GET,
            handler: this.get_all,
        },
        {
            path: '/search/:name',
            method: RouteMethodType.GET,
            handler: this.search_by_name,
        },
        {
            path: '/edit/:id',
            method: RouteMethodType.POST,
            handler: this.edit_by_id,
        },
        {
            path: '/add',
            method: RouteMethodType.POST,
            handler: this.add,
        },
    ];

    async add(req: Request, res: Response): Promise<void> {
        const data: Spell = req.body;

        const spell: SpellDocument = new SpellObj();
        spell.source_book = data.source_book;
        spell.level = data.level;

        spell.name = data.name;

        spell.school = data.school;
        spell.classes = data.classes;
        spell.ritual = data.ritual;

        spell.time_consumption = data.time_consumption;

        spell.range = data.range;

        spell.target = data.target;
        spell.components = data.components;

        spell.duration = data.duration;

        spell.description = data.description;
        spell.higher_levels = data.higher_levels;
        spell.attributes = data.attributes;

        spell.save().then(saved => {
            if (saved === spell) {
                res.json(spell);
            } else {
                res.json({ success: false });
            }
        });
    }

    async edit_by_id(req: Request, res: Response): Promise<void> {
        const filter = {
            _id: req.params.id,
        };
        console.log(req.body);
        await SpellObj.findOneAndUpdate(filter, req.body, {
            setDefaultsOnInsert: false,
        }).exec();
        res.json(req.body);
    }

    async search_by_name(req: Request, res: Response): Promise<void> {
        const name = decodeURI(req.params.name).toLowerCase();
        console.log(name);
        const spells = await SpellObj.find({
            $or: [
                {
                    'name.english': {
                        $regex: name,
                        $options: 'i',
                    },
                },
                {
                    'name.german': {
                        $regex: name,
                        $options: 'i',
                    },
                },
            ],
        }).select({ __v: 0 }).lean().exec();
        res.send(spells);
    }

    async get_all(req: Request, res: Response): Promise<void> {
        const spells = await SpellObj.find().select({ __v: 0 }).lean().exec();
        res.send(spells);
    }
}

export { SpellsRoute };
